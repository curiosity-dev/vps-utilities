# What to do in case 1 or more VPS(s) need to reboot

From time to time, you may need to reboot 1 or more of our VPS(s). In this case, you need to make sure that when the VPS starts back up, all the services are loaded.

Currently, you need to manually enable the following services for each VPS.

| VM Name            | OS         | Process Management Software | Services which will autorestart | Services which need to manually be started |
|--------------------|------------|-----------------------------|---------------------------------|--------------------------------------------|
| brainclub          | Arch Linux | pm2                         | HttpCatsBot                     | None                                       |
| internalgit-vps    | Debian     | systemd                     | GitLab                          | None                                       |
| passmgr            | Debian     | systemd                     | Passbolt                        | None                                       |
| internal-buildkite | Ubuntu     | systemd                     | Buildkite Agent                 | None                                       |
