# Updating GitLab

Sometimes you might see the need to update GitLab (especially if the Admin Area says `update asap`). Here's a simple guide on how to do that.

## If you can SSH
- SSH into the internalgit-vps instance.
- Make sure you have sudo permissions
- Run `sudo apt-get update`
- After that run `sudo apt-get install gitlab-ce`
- Wait a while, it should update.

## If you cannot SSH
- Contact a Server Wrangler on Discord
- Ask them to update the GitLab
- Tell Server Wrangler to follow above steps

**In case the GitLab instance misbehaves after updating**
- Run `sudo gitlab-ce restart` and wait for it to restart. Do this before doing anything else, if you suspect the instance is misbehaving.
  - Note that it may return a 502 for a few moments after updating, this is normal. If this proceeds for longer than 30 minutes though, then run the above command.
