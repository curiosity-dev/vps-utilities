# Using the scripts (in `/scripts`)

In case you ever need to use one of the scripts in the scripts directory of this repository, here's the documentation for each script.

## aurinstall

**Description**: This script downloads, builds, and installs any package from the Arch User Repository (AUR).

**Usage**:
```
$ ./aurinstall.sh <package name>
```

## backuputils

**Description**: This script manages backing up and restoring servers.

**Usage**:
```
$ sudo ./backuputils.sh <action>
$ sudo ./backuputils.sh backup # example for backing up the entire system
$ sudo ./backuputils.sh restore # example for restoring from backup
```

## ispkginstalled

**Description**: Checks if a package is installed.

**Usage**:
```
$ ./ispkginstalled.sh base-devel
Package is installed.
$ ./ispkginstalled.sh thisisjustatestrepositorynamesopleasedontthinkthisisarealrepository
Package is not installed.
```

## swapthief

**Description**: Checks what is using your swap memory currently.

**Usage**:
```
$ ./swapthief.sh
```

## useless-pkgs

**Description**: Checks which packages are not used as dependencies by other installed packages and logs them to stdout.

**Usage**:
```
$ ./useless-pkgs.sh
```

**Extra Information**: If you add a package name to `/tmp/ignored` or `/tmp/installed` it will be excluded from showing in stdout.
