# Currently provisioned instances

## Last updated: `April 20, 2018` at `6:50 PM EST`

| Server Name     	| Description                                                 	| OS         | Status      |
|-----------------	|-------------------------------------------------------------	|------------|-------------|
| brainclub       	| Used for small scripts and Discord bot hosting.             	| Arch Linux | **Retired** |
| internalgit-vps 	| Powers intgit.curiositydev.me                               	| Debian     | **Retired** |
| passmgr         	| Powers pwm.curiositydev.me - our internal password manager. 	| Debian     | **Retired** |
| internal-buildkite| Powers our internal Buildkite CI solution.                    | Ubuntu     | **Retired** |
