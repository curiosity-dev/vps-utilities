# Setting up pacman for the first time

After you provision a new Compute instance with Arch Linux, you will need to follow the following steps to install a package for the first time:

1. In the SSH shell, run `sudo pacman-key --init`. This will initialize the keyring.
2. Then, update the `archlinux-keyring` package via pacman. `sudo pacman -S archlinux-keyring`
3. Try to install a package. It might error with `Corrupt package` errors or something similar.
4. After it errors, run step 2 again.
5. Now try to install the package again. It should work this time and you shouldn't need to run this ever again in the future on the current instance.
