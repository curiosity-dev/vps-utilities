# Backing up the VPS(s)

Backups are important. They help a lot in case everything goes bad. Because of this, we have a nice little system when it comes to backing up our servers.

We use the `backuputils` script to back up our servers.

We also use a Cron job to automate this backup, it runs every day at midnight, however if there is an existing backup younger than 30 days then it will not backup again.

We can use the same script to restore a backup.

Backups are made using `duplicity`.

## Requirements
- Python 2.7
- pip for Python 2.7 (using the get-pip.py script found [here](https://gist.githubusercontent.com/Hexexpeck/2507c214ccf9a1dc9b85fd8fd044c5cf/raw/545540b380b773fbbcb10b4126c0a480147b76b4/get-pip.py))
- `duplicity` package installed via pacman (`sudo pacman -Syu duplicity`)
- `boto` package installed via pip (`sudo pip install boto` or `sudo python2.7 -m pip install boto`)
- Properly setup `/env/environment` file.
  - If backing up: your `/env/environment` file needs to include these 3 environment variables:
    ```
    GS_ACCESS_KEY_ID=insert your google cloud storage access key here
    GS_SECRET_ACCESS_KEY=insert your google cloud storage secret key here
    PASSPHRASE=random secure passphrase used to encrypt/decrypt the backup
    ```
  - If restoring: the `PASSPHRASE` needs to be the same in your `env.txt` as it was in `/env/environment` in order to decrypt the backup.

## Backing up manually
**Note**: Backups can take a while, especially when you're backing up almost everything (except for the stuff listed below) so be patient.

If for whatever reason you need to manually backup, run the `backuputils` script.

Make sure you already installed Python 2.7, pip, boto (via pip), and duplicity (via pacman).
```
# backuputils.sh backup
```
This will backup everything to the GCP storage bucket if there is no existing backup. If a backup exists, it must be older than 30 days for this to back it up again.

## Restoring backups
To restore a backup, there is a little manual intervention.

Make sure you have Python 2.7, pip, boto (via pip), and duplicity (via pacman).

Then, you run the `backuputils` script on the server to restore the backup to, like so:
```
# backuputils restore
```
Running it the first time will create an env.txt file. Populate this env.txt file with your Google Cloud Platform Storage access keys
as well as the passphrase used to encrypt the backup.

After you've populated the file, run this script again and it should restore the backup. Make sure to run it as root!


## Files and directories not backed up
- `/dev/*`
- `/mnt/*`
- `/var/*`
- `/media/*`
- `/tmp/*`
- `/lost+found/*`
- `/proc/*`
- `/sys/*`
- `/run/*`

If you need to backup any of these directories for some reason then DM `Hexexpeck#0001` via Discord.
