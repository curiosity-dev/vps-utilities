# In case of emergency (VPS goes down, VPS gets deleted, etc)

Stuff like this has happened to us before. I accidentally deleted the VPS once and had to rebuild everything from scratch with a new VPS.

Let's try to prevent that from happening.

## If 1 or more VPS'es go(es) down

If you suspect that a VPS is down (you can't SSH/SFTP into it, content served by it isn't loading such as webpages, bots, etc) then:

- Immediately ping the Server Wranglers role in the Discord. (such as `Hexexpeck#0001`)
- Let them know of the issue (what are you trying to do?, when did this start?, etc)
- They will check the Google Cloud Platform status to see what's going on.
- They will post an update on the CuriosityDev Discord stating what is going on.
- If they have the ability to fix it, they will attempt to do so.

**Server Wranglers**:
If you have deployed a fix, post a new announcement on the Discord to inform users.

If a fix is not possible, post a new announcement on the Discord, giving more information on what the issue is (such an issue on Google's end) and then DM `Hexexpeck#0001` to let him know of the incident.

You are only allowed to use `@everyone`/`@here` pings in the Discord if the incident cannot be fixed within at least 5 hours. Otherwise, just post a regular update.

## If 1 or more VPS'es get(s) deleted

There are no surefire signs to know if a VPS is deleted or just down, but they are pretty much the similar:
- You cannot SSH or SFTP into the VPS
- Websites/bots/other stuff which is hosted on the VPS is no longer accessible

Either way, immediately tell a VPS Admin/Server Wrangler. They will check if the VPS is deleted.

**Server Wranglers**:
If the VPS is deleted, immediately notify `Hexexpeck#0001` over Discord. A group DM will be created which will include all the Server Wranglers so we can work together on bringing back the server. Usually, we will restore the newest backup published to the GCP storage. If a backup restoration is not possible, we will work on other solutions.

## If the server breaks during maintenance/upgrades

If the server does break during maintenance (scheduled or emergency) or during upgrades, then follow these steps:

- Immediately head to the secret Server Wranglers chat on Discord and ping the Server Wranglers role
- Let them know that the server went down during maintenance and what kind of maintenance was going on.
- If you can, then try to restore the backup made prior to beginning the maintenance.
  - If you cannot, then ask another Server Wrangler to do so.
- If you cannot restore a backup, DM `Hexexpeck#0001` immediately letting him know of the issue.
- Hexexpeck will create a group DM, invite all stakeholders, and discuss possible recovery options.
- Server restoration and recovery will proceed from there, using whatever option is globally voted on in the group DM.
