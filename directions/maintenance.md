# Maintaining the VPS'es
In order to make sure our VPS'es don't go down or get deleted, we need to perform regular maintenance. This maintenance also enhances
the user experience if the VPS serves content such as websites, bots, or other things.

Here's some guidelines to follow when performing maintenance.

Generally we don't have any commands we are required to run before we can perform maintenance on the VPS.

## General maintenance steps
- Ask for permission to perform maintenance from a fellow Server Wrangler on the Discord.
- If granted permission, go ahead and publically announce scheduled maintenance and on which date it will be occuring.
  - If maintenance is emergency maintenance: publically announce it and describe that it is emergency and not scheduled maintenance and service downtime will happen immediately.
- Go ahead and SSH into the VPS to perform maintenance on.
- Manually run the backup script to create a backup of the VPS.
- Perform your maintenance (make sure it follows Rule 9!).
- When maintenance is complete, ask a fellow Server Wrangler to make sure everything is OK.
- Announce the maintenance completion on the Discord publically.
- Complete.

If there are critical errors during maintenance:
- Follow rule 8 below.

## Performing general maintenance
When performing general maintenance, always manually run the backup script. This ensures that if you mess up, you have a backup of the VPS
prior to your mess-up.

Also please notify fellow Server Wranglers of the planned maintenance via Discord.

We recommend following the [Arch Linux Maintenance Guide](https://wiki.archlinux.org/index.php/System_maintenance) as well.

## Performing system upgrades
**You must advise fellow Server Wranglers of any upgrades to the system or crucial system components before doing them.**
If a Server Wrangler says you cannot proceed with the upgrade, do not upgrade.

When upgrading the VPS, it is **crucial** that you advise fellow Server Wranglers *before* you upgrade.

Also, please make a manual backup using the backup script. This way if you bork the system while upgrading, you can safely revert.

We recommend following the [Arch Linux Upgrade Guide](https://wiki.archlinux.org/index.php/System_maintenance#Read_before_upgrading_the_system) when upgrading.

All maintenance is subject to the following rules:

## Rules of Maintenance

- **If it doesn't need fixing and works good enough, don't try to fix it.**
- Always make manual backups using the backup script **before** performing any type of maintenance.
- Prior to scheduled maintenance, you must inform the Discord via the announcements channel.
- Prior to any form of maintenance, you must inform at least 1 fellow Server Wrangler via the Discord.
- If at least 1 fellow Server Wrangler says you may *not* proceed with maintenance, **do not proceed with maintenance.**
- If you encounter an error, post it in the Discord. A fellow Server Wrangler will try their best to help you.
- If nobody can help, you are welcome to ask the public for help but make sure any sensitive information is omitted from your request.
- If you break the server to the point where a format or other major action is required to fix it, follow the [In Case Of Emergency Guide](https://github.com/CuriosityDev/vps-utilities/blob/master/directions/in-case-of-emergency.md)
- Running stuff like `sudo rm -rf --no-preserve-root /` or other potentially destructive administrative commands is not allowed on our VPS and is grounds for you getting removed from the CuriosityDev team. **Only run these commands if given explicit permission from `Hexexpeck#0001` and him only.**
