#!/bin/bash
if [[ $# -eq 0 ]]
 then echo "Missing package name argument." && exit 1;
fi
if [ -z "$1" ]
 then echo "Invalid package name argument." && exit 1;
fi
x=`pacman -Qs $1`
if [ -n "$x" ]
 then echo "Package is installed"
 else echo "Package is not installed"
fi
