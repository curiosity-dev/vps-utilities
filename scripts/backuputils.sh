#!/bin/bash
if [[ $EUID -ne 0 ]]; then
  echo "The backup script has to be run as root. Run it as root and try again."
  exit 1
fi

if [[ $# -eq 0 ]] ; then
    echo 'Too few arguments - you must specify either backup or restore as an argument.'
    exit 1
fi

if [ -z "$1" ]
  then
    echo "Invalid argument - you must specify either backup or restore as an argument."
fi

hname=$(hostname -s);

if [[ $1 -eq "backup" ]]; then
  duplicity --exclude "**/dev/**" --exclude "**/mnt/**" --exclude "**/var/**" --exclude "**/media/**" --exclude "**/tmp/**" --exclude "**/lost+found/**" --exclude "**/proc/**" --exclude "**/sys/**" --exclude "**/run/**" --archive-dir /root/.cache/duplicity/ --full-if-older-than 1M / gs://cdev-vps-backups/$hname/ >> /var/log/duplicity.log
  exit 0;
fi

if [[ $1 -eq "restore" ]]; then
  echo Restoring...
  if [ ! -f ./env.txt ]; then
    echo "Failed to find env.txt file.";
    echo ""
    echo "Please edit the newly created env.txt file and add in your"
    echo "GCP storage access keys and a secure passphrase to restore."
    echo ""
    echo "After that, run this script again."
    touch ./env.txt
    echo "export GS_ACCESS_KEY_ID=" >> env.txt;
    echo "" >> env.txt;
    echo "export GS_SECRET_ACCESS_KEY=" >> env.txt;
    echo "" >> env.txt;
    echo "export PASSPHRASE=" >> env.txt;
    exit 1;
  else
    echo "Found env.txt file, sourcing...";
    source env.txt;
    echo "Now restoring...";
    sudo -E duplicity restore --archive-dir /root/.cache/duplicity gs://cdev-vps-backups/$hname/ /
    exit 0;
  fi
fi
