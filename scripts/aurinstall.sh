#!/bin/bash
if [[ $# -eq 0 ]]; then
	echo 'Too few arguments - you must specify a package to install from the AUR.';
	exit 1;
fi

if [ -z "$1" ]; then
	echo "Invalid argument - you must specify a package to install from the AUR.";
	exit 1;
fi

# check if base-devel is installed so we can build the downloaded packages from AUR
echo "Checking if the base-devel package is installed (required to build AUR packages)...";
if [ pacman -Qi base-devel ]; then
	echo "Missing base-devel package, automatically installing...";
	sudo pacman -Syu base-devel;
else
	echo "base-devel package is installed!";
fi

# if the AURINSTALL_DIRECTORY variable is set then download all AUR packages into the value for it
if [ -z "$AURINSTALL_DIRECTORY" ]; then
	git clone https://aur.archlinux.org/$1.git && cd $1 && makepkg -si && exit 0 || echo "Error installing package from AUR." && exit 1
else
	cd $AURINSTALL_DIRECTORY && git clone https://aur.archlinux.org/$1.git && cd $1 && makepkg -si && exit 0 || echo "Error installing package from AUR." && exit 1
fi
